package ru.t1.dkozoriz.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.dkozoriz.tm.api.repository.ISessionRepository;
import ru.t1.dkozoriz.tm.model.Session;
import ru.t1.dkozoriz.tm.model.business.Project;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class SessionRepositoryTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private static final String USER_ID_1 = UUID.randomUUID().toString();

    @NotNull
    private static final String USER_ID_2 = UUID.randomUUID().toString();

    @NotNull
    private final List<Session> sessionList = new ArrayList<>();

    @NotNull
    private final ISessionRepository sessionRepository = new SessionRepository();

    @Before
    public void initRepository() {
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final Session session = new Session();
            if (i <= 5) session.setUserId(USER_ID_1);
            else session.setUserId(USER_ID_2);
            sessionRepository.add(session);
            sessionList.add(session);
        }
    }

    @Test
    public void testAddSession() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        @NotNull final Session session = new Session();
        sessionRepository.add(session);
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.getSize());
    }

    @Test
    public void testAddSessionUserOwned() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        @NotNull final Session session = new Session();
        sessionRepository.add(USER_ID_1, session);
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.getSize());
        Assert.assertNull(sessionRepository.add(null, session));
    }

    @Test
    public void testAddSessionCollection() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES + 2;
        @NotNull List<Session> sessions = new ArrayList<>();
        @NotNull final Session session1 = new Session();
        @NotNull final Session session2 = new Session();
        sessions.add(session1);
        sessions.add(session2);
        sessionRepository.add(sessions);
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.getSize());
    }

    @Test
    public void testClearSession() {
        final int expectedNumberOfEntries = 0;
        sessionRepository.clear();
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.getSize());
    }

    @Test
    public void testClearUserOwnedSession() {
        @NotNull final List<Session> userSessionList = sessionList
                .stream()
                .filter(p -> USER_ID_1.equals(p.getUserId()))
                .collect(Collectors.toList());
        final int expectedNumberOfEntries = userSessionList.size();
        sessionRepository.clear(USER_ID_1);
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.getSize());
    }

    @Test
    public void testGetSizeSession() {
        Assert.assertEquals(sessionList.size(), sessionRepository.getSize());
    }

    @Test
    public void testGetSizeUserOwnedSession() {
        @NotNull final List<Session> userSessionList = sessionList
                .stream()
                .filter(p -> USER_ID_1.equals(p.getUserId()))
                .collect(Collectors.toList());
        Assert.assertEquals(userSessionList.size(), sessionRepository.getSize(USER_ID_1));
    }

    @Test
    public void testSetSession() {
        @NotNull List<Session> sessions = new ArrayList<>();
        @NotNull final Session session1 = new Session();
        @NotNull final Session session2 = new Session();
        sessions.add(session1);
        sessions.add(session2);
        final int expectedNumberOfEntries = sessions.size();
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.set(sessions).size());
    }

    @Test
    public void testFindAllSession() {
        Assert.assertEquals(sessionList.size(), sessionRepository.findAll().size());
    }

    @Test
    public void testFindAllUserOwnedSession() {
        @NotNull final List<Session> userSessionList = sessionList
                .stream()
                .filter(p -> USER_ID_1.equals(p.getUserId()))
                .collect(Collectors.toList());
        final int expectedNumberOfEntries = userSessionList.size();
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.findAll(USER_ID_1).size());
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.findAll(USER_ID_1).size());
        Assert.assertEquals(Collections.emptyList(), sessionRepository.findAll(null));
    }

    @Test
    public void testFindSessionById() {
        for (@NotNull final Session session : sessionList) {
            Assert.assertNotNull(sessionRepository.findById(session.getId()));
            Assert.assertEquals(session, sessionRepository.findById(session.getId()));
        }
    }

    @Test
    public void testFindSessionByIndex() {
        for (int i = 0; i < sessionRepository.getSize(); i++) {
            Assert.assertNotNull(sessionRepository.findByIndex(i));
            Assert.assertEquals(sessionList.get(i), sessionRepository.findByIndex(i));
        }
    }

    @Test
    public void testFindUserOwnedSessionById() {
        @NotNull final List<Session> userSessionList = sessionList
                .stream()
                .filter(p -> USER_ID_1.equals(p.getUserId()))
                .collect(Collectors.toList());
        for (@NotNull final Session session : userSessionList) {
            Assert.assertNotNull(sessionRepository.findById(USER_ID_1, session.getId()));
            Assert.assertEquals(session, sessionRepository.findById(USER_ID_1, session.getId()));
        }
    }

    @Test
    public void testFindUserOwnedSessionByIndex() {
        @NotNull final List<Session> userSessionList = sessionList
                .stream()
                .filter(p -> USER_ID_1.equals(p.getUserId()))
                .collect(Collectors.toList());

        for (int i = 0; i < userSessionList.size(); i++) {
            Assert.assertNotNull(sessionRepository.findByIndex(USER_ID_1, i));
            Assert.assertEquals(sessionList.get(i), sessionRepository.findByIndex(USER_ID_1, i));
        }
    }

    @Test
    public void testExistUserOwnedSessionById() {
        @NotNull final List<Session> userSessionList = sessionList
                .stream()
                .filter(p -> USER_ID_1.equals(p.getUserId()))
                .collect(Collectors.toList());
        for (@NotNull final Session session : userSessionList) {
            Assert.assertTrue(sessionRepository.existById(USER_ID_1, session.getId()));
        }
    }

    @Test
    public void testRemoveSession() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        @NotNull final Session session = sessionList.get(0);
        sessionRepository.remove(session);
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.getSize());
        @Nullable final Session session2 = null;
        Assert.assertNull(sessionRepository.remove(session2));
    }

    @Test
    public void testRemoveSessionById() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        @NotNull final Session session = sessionList.get(0);
        sessionRepository.removeById(session.getId());
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.getSize());
    }

    @Test
    public void testRemoveSessionByIndex() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        final int index = 0;
        sessionRepository.removeByIndex(index);
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.getSize());
    }

    @Test
    public void testRemoveUserOwnedSession() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        @NotNull final Session session = sessionList.get(0);
        sessionRepository.remove(USER_ID_1, session);
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.getSize());
        Assert.assertNull(sessionRepository.remove(null, sessionList.get(1)));
        Assert.assertNull(sessionRepository.remove("", sessionList.get(2)));
    }

    @Test
    public void testRemoveUserOwnedSessionById() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        @NotNull final Session session = sessionList.get(0);
        sessionRepository.removeById(USER_ID_1, session.getId());
        Assert.assertNull(sessionRepository.removeById(null, session.getId()));
        Assert.assertNull(sessionRepository.removeById("", session.getId()));
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.getSize());
    }

    @Test
    public void testRemoveUserOwnedSessionByIndex() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        final int index = 0;
        sessionRepository.removeByIndex(USER_ID_1, index);
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.getSize());
    }
    
}
