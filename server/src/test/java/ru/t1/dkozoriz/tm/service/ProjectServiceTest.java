package ru.t1.dkozoriz.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.dkozoriz.tm.api.repository.business.IProjectRepository;
import ru.t1.dkozoriz.tm.api.service.business.IProjectService;
import ru.t1.dkozoriz.tm.enumerated.Sort;
import ru.t1.dkozoriz.tm.enumerated.Status;
import ru.t1.dkozoriz.tm.exception.entity.EntityException;
import ru.t1.dkozoriz.tm.exception.field.*;
import ru.t1.dkozoriz.tm.exception.user.UserIdEmptyException;
import ru.t1.dkozoriz.tm.model.business.Project;
import ru.t1.dkozoriz.tm.repository.business.ProjectRepository;
import ru.t1.dkozoriz.tm.service.business.ProjectService;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class ProjectServiceTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private static final String USER_ID_1 = UUID.randomUUID().toString();

    @NotNull
    private static final String USER_ID_2 = UUID.randomUUID().toString();

    @NotNull
    private final List<Project> projectList = new ArrayList<>();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @Before
    public void initRepository() {
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final Project project = new Project();
            project.setName("Project " + i);
            project.setDescription("Description " + i);
            if (i <= 5) project.setUserId(USER_ID_1);
            else project.setUserId(USER_ID_2);
            projectRepository.add(project);
            projectList.add(project);
        }
    }

    @Test
    public void testAddProject() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        @NotNull final Project project = new Project();
        projectService.add(project);
        Assert.assertEquals(expectedNumberOfEntries, projectService.getSize());
    }

    @Test
    public void testAddProjectNegative() {
        @Nullable final Project project = null;
        Assert.assertThrows(EntityException.class, () -> projectService.add(project));
    }

    @Test
    public void testClearProject() {
        final int expectedNumberOfEntries = 0;
        projectService.clear();
        Assert.assertEquals(expectedNumberOfEntries, projectService.getSize());
    }

    @Test
    public void testClearUserOwnedProject() {
        @NotNull final List<Project> userProjectList = projectList
                .stream()
                .filter(p -> USER_ID_1.equals(p.getUserId()))
                .collect(Collectors.toList());
        final int expectedNumberOfEntries = userProjectList.size();
        projectService.clear(USER_ID_1);
        Assert.assertEquals(expectedNumberOfEntries, projectService.getSize());
    }

    @Test
    public void testClearUserOwnedProjectNegative() {
        @Nullable final String userId1 = null;
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.clear(userId1));
        @Nullable final String userId2 = "";
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.clear(userId2));
    }

    @Test
    public void testFindAllProject() {
        Assert.assertEquals(projectList.size(), projectService.findAll().size());
    }

    @Test
    public void testFindAllUserOwnedProject() {
        @NotNull final List<Project> userProjectList = projectList
                .stream()
                .filter(p -> USER_ID_1.equals(p.getUserId()))
                .collect(Collectors.toList());
        final int expectedNumberOfEntries = userProjectList.size();
        Assert.assertEquals(expectedNumberOfEntries, projectService.findAll(USER_ID_1).size());
    }

    @Test
    public void testFindAllProjectSort() {
        @NotNull final List<Project> userProjectList = projectList
                .stream()
                .filter(p -> USER_ID_1.equals(p.getUserId()))
                .collect(Collectors.toList());
        final int expectedNumberOfEntries = userProjectList.size();
        final Sort sort = Sort.toSort("BY_NAME");
        Assert.assertEquals(expectedNumberOfEntries, projectService.findAll(USER_ID_1, sort).size());
    }

    @Test
    public void testFindAllUserOwnedProjectNegative() {
        @Nullable final String userId = null;
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.findAll(userId));
    }

    @Test
    public void testFindAllProjectSortNegative() {
        @Nullable final String userId = null;
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.findAll(userId, Sort.toSort("BY_NAME")));
    }

    @Test
    public void testRemoveProject() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        @NotNull final Project project = projectList.get(0);
        projectService.remove(project);
        Assert.assertEquals(expectedNumberOfEntries, projectService.getSize());
    }

    @Test
    public void testRemoveProjectNegative() {
        @Nullable final Project project = null;
        Assert.assertThrows(EntityException.class, () -> projectService.remove(project));
    }

    @Test
    public void testRemoveUserOwnedProject() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        @NotNull final Project project = projectList.get(0);
        projectRepository.remove(USER_ID_1, project);
        Assert.assertEquals(expectedNumberOfEntries, projectService.getSize());
    }

    @Test
    public void testRemoveUserOwnedProjectNegative() {
        @Nullable final String userId = null;
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.remove(userId, projectList.get(0)));
        @Nullable final Project project = null;
        Assert.assertThrows(EntityException.class, () -> projectService.remove(USER_ID_1, project));
    }

    @Test
    public void testRemoveProjectById() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        @NotNull final Project project = projectList.get(0);
        projectService.removeById(project.getId());
        Assert.assertEquals(expectedNumberOfEntries, projectService.getSize());
    }

    @Test
    public void testRemoveProjectByIdNegative() {
        @Nullable final String id1 = null;
        Assert.assertThrows(IdEmptyException.class, () -> projectService.removeById(id1));
        @Nullable final String id2 = "";
        Assert.assertThrows(IdEmptyException.class, () -> projectService.removeById(id2));
    }

    @Test
    public void testRemoveUserOwnedProjectById() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        @NotNull final Project project = projectList.get(0);
        projectService.removeById(USER_ID_1, project.getId());
        Assert.assertEquals(expectedNumberOfEntries, projectService.getSize());
    }

    @Test
    public void testRemoveUserOwnedProjectByIdNegative() {
        @Nullable final String userId = null;
        @Nullable final String projectId = projectList.get(0).getId();
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.removeById(userId, projectId));
        @Nullable final String id = null;
        Assert.assertThrows(IdEmptyException.class, () -> projectService.existsById(USER_ID_1, id));
    }

    @Test
    public void testRemoveUserOwnedProjectByIndex() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        final int index = 0;
        projectService.removeByIndex(USER_ID_1, index);
        Assert.assertEquals(expectedNumberOfEntries, projectService.getSize());
    }

    @Test
    public void testRemoveUserOwnedProjectByIndexNegative() {
        @Nullable final String userId = null;
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.removeByIndex(userId, 0));
        @Nullable final Integer index = -1;
        Assert.assertThrows(IndexIncorrectException.class, () -> projectService.removeByIndex(USER_ID_1, index));
    }


    @Test
    public void testFindProjectById() {
        for (@NotNull final Project project : projectList) {
            Assert.assertNotNull(projectService.findById(project.getId()));
            Assert.assertEquals(project, projectService.findById(project.getId()));
        }
    }

    @Test
    public void testFindUserOwnedProjectById() {
        @NotNull final List<Project> userProjectList = projectList
                .stream()
                .filter(p -> USER_ID_1.equals(p.getUserId()))
                .collect(Collectors.toList());
        for (@NotNull final Project project : userProjectList) {
            Assert.assertNotNull(projectService.findById(USER_ID_1, project.getId()));
            Assert.assertEquals(project, projectService.findById(USER_ID_1, project.getId()));
        }
    }

    @Test
    public void testFindUserOwnedProjectByIdNegative() {
        @Nullable final String userId = null;
        @Nullable final String projectId = projectList.get(0).getId();
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.findById(userId, projectId));
        @Nullable final String id = null;
        Assert.assertThrows(IdEmptyException.class, () -> projectService.findById(USER_ID_1, id));
    }

    @Test
    public void testFindProjectByIdNegative() {
        @Nullable final String id = null;
        Assert.assertThrows(IdEmptyException.class, () -> projectService.findById(id));
    }

    @Test
    public void testFindUserOwnedProjectByIndex() {
        @NotNull final List<Project> userProjectList = projectList
                .stream()
                .filter(p -> USER_ID_1.equals(p.getUserId()))
                .collect(Collectors.toList());

        for (int i = 0; i < userProjectList.size(); i++) {
            Assert.assertNotNull(projectService.findByIndex(USER_ID_1, i));
            Assert.assertEquals(projectList.get(i), projectService.findByIndex(USER_ID_1, i));
        }
    }

    @Test
    public void testFindUserOwnedProjectByIndexNegative() {
        @Nullable final String userId = null;
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.findByIndex(userId, 0));
        @Nullable final Integer index = null;
        Assert.assertThrows(IndexIncorrectException.class, () -> projectService.findByIndex(USER_ID_1, index));
    }

    @Test
    public void testGetSizeProject() {
        Assert.assertEquals(projectList.size(), projectService.getSize());
    }

    @Test
    public void testGetSizeUserOwnedProject() {
        @NotNull final List<Project> userProjectList = projectList
                .stream()
                .filter(p -> USER_ID_1.equals(p.getUserId()))
                .collect(Collectors.toList());
        Assert.assertEquals(userProjectList.size(), projectService.getSize(USER_ID_1));
    }

    @Test
    public void testSetProject() {
        @NotNull List<Project> projects = new ArrayList<>();
        @NotNull final Project project1 = new Project();
        @NotNull final Project project2 = new Project();
        projects.add(project1);
        projects.add(project2);
        final int expectedNumberOfEntries = projects.size();
        Assert.assertEquals(expectedNumberOfEntries, projectService.set(projects).size());
    }

    @Test
    public void testAddProjectCollection() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES + 2;
        @NotNull List<Project> projects = new ArrayList<>();
        @NotNull final Project project1 = new Project();
        @NotNull final Project project2 = new Project();
        projects.add(project1);
        projects.add(project2);
        projectService.add(projects);
        Assert.assertEquals(expectedNumberOfEntries, projectService.getSize());
    }

    @Test
    public void testExistProjectById() {
        @NotNull final List<Project> userProjectList = projectList
                .stream()
                .filter(p -> USER_ID_1.equals(p.getUserId()))
                .collect(Collectors.toList());
        for (@NotNull final Project project : userProjectList) {
            Assert.assertTrue(projectService.existsById(USER_ID_1, project.getId()));
        }
    }

    @Test
    public void testExistProjectByIdNegative() {
        @Nullable final String userId = null;
        @Nullable final String projectId = projectList.get(0).getId();
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.existsById(userId, projectId));
        @Nullable final String id = null;
        Assert.assertThrows(IdEmptyException.class, () -> projectService.existsById(USER_ID_1, id));
    }

    @Test
    public void testChangeStatusById() {
        @Nullable final Project project = projectService
                .changeStatusById(USER_ID_1, projectList.get(0).getId(), Status.IN_PROGRESS);
         @Nullable final Status status = project.getStatus();
        Assert.assertEquals(Status.IN_PROGRESS, status);
    }

    @Test
    public void testChangeStatusByIdNegative() {
        @Nullable final String userId = null;
        @Nullable final String projectId = projectList.get(0).getId();
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService
                .changeStatusById(userId, projectId, Status.IN_PROGRESS));
        @Nullable final String id = null;
        Assert.assertThrows(IdEmptyException.class, () -> projectService
                .changeStatusById(USER_ID_1, id, Status.IN_PROGRESS));
        @Nullable final Status status = null;
        Assert.assertThrows(StatusEmptyException.class, () -> projectService
                .changeStatusById(USER_ID_1, projectId, status));
    }

    @Test
    public void testChangeStatusByIndex() {
        @Nullable final Project project = projectService
                .changeStatusByIndex(USER_ID_1, 0, Status.IN_PROGRESS);
        @Nullable final Status status = project.getStatus();
        Assert.assertEquals(Status.IN_PROGRESS, status);
    }

    @Test
    public void testChangeStatusByIndexNegative() {
        @Nullable final String userId = null;
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService
                .changeStatusByIndex(userId, 0, Status.IN_PROGRESS));
        @Nullable final Integer index = null;
        Assert.assertThrows(IndexIncorrectException.class, () -> projectService
                .changeStatusByIndex(USER_ID_1, index, Status.IN_PROGRESS));
        @Nullable final Status status = null;
        Assert.assertThrows(StatusEmptyException.class, () -> projectService
                .changeStatusByIndex(USER_ID_1, 0, status));
    }

    @Test
    public void testUpdateById() {
        @Nullable final Project project = projectList.get(0);
        @Nullable final String name = "new name";
        @Nullable final String description = "new description";
        Assert.assertEquals(name, projectService.updateById(USER_ID_1, project.getId(), name, description).getName());
        Assert.assertEquals(description, projectService.updateById(USER_ID_1, project.getId(), name, description).getDescription());
    }

    @Test
    public void testUpdateByIdNegative() {
        @Nullable final String userId = null;
        @Nullable final String projectId = projectList.get(0).getId();
        @Nullable final String name = "new name";
        @Nullable final String description = "new description";
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService
                .updateById(userId, projectId, name, description));
        @Nullable final String id = null;
        Assert.assertThrows(IdEmptyException.class, () -> projectService
                .updateById(USER_ID_1, id, name, description));
        @Nullable final String name2 = null;
        Assert.assertThrows(NameEmptyException.class, () -> projectService
                .updateById(USER_ID_1, projectId, name2, description));
    }

    @Test
    public void testUpdateByIndex() {
        @Nullable final Integer index = 0;
        @Nullable final String name = "new name";
        @Nullable final String description = "new description";
        Assert.assertEquals(name, projectService.updateByIndex(USER_ID_1, index, name, description).getName());
        Assert.assertEquals(description, projectService.updateByIndex(USER_ID_1, index, name, description).getDescription());
    }

    @Test
    public void testUpdateByIndexNegative() {
        @Nullable final String userId = null;
        @Nullable Integer index = 0;
        @Nullable final String name = "new name";
        @Nullable final String description = "new description";
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService
                .updateByIndex(userId, index, name, description));
        @Nullable final Integer index2 = null;
        Assert.assertThrows(IndexIncorrectException.class, () -> projectService
                .updateByIndex(USER_ID_1, index2, name, description));
        @Nullable final String name2 = null;
        Assert.assertThrows(NameEmptyException.class, () -> projectService
                .updateByIndex(USER_ID_1, index, name2, description));
    }

    @Test
    public void testCreateProject() {
        @Nullable final String name = "name";
        @Nullable final String description = "description";
        @Nullable final Project project = new Project(name);
        project.setDescription(description);
        Assert.assertEquals(project.getName(), projectService.create(USER_ID_1, name, description).getName());
        Assert.assertEquals(project.getDescription(), projectService.create(USER_ID_1, name, description).getDescription());
    }

    @Test
    public void testCreateProjectNegative() {
        @Nullable final String userId = null;
        @Nullable final String name = "name";
        @Nullable final String description = "description";
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService
                .create(userId, name, description));
        @Nullable final String name2 = null;
        Assert.assertThrows(NameEmptyException.class, () -> projectService
                .create(USER_ID_1, name2, description));
        @Nullable final String description2 = null;
        Assert.assertThrows(DescriptionEmptyException.class, () -> projectService
                .create(USER_ID_1, name, description2));
    }

}