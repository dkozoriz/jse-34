package ru.t1.dkozoriz.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.dkozoriz.tm.api.repository.IUserRepository;
import ru.t1.dkozoriz.tm.api.service.IPropertyService;
import ru.t1.dkozoriz.tm.model.User;
import ru.t1.dkozoriz.tm.model.business.Project;
import ru.t1.dkozoriz.tm.service.PropertyService;
import ru.t1.dkozoriz.tm.util.HashUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class UserRepositoryTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private final List<User> userList = new ArrayList<>();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Before
    public void initRepository() {
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final User user = new User();
            user.setLogin("LOGIN " + i);
            user.setPasswordHash(HashUtil.salt(propertyService, "password" + i));
            user.setEmail("mail@" + i);
            userRepository.add(user);
            userList.add(user);
        }
    }

    @Test
    public void testAddUser() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        @NotNull final User user = new User();
        userRepository.add(user);
        Assert.assertEquals(expectedNumberOfEntries, userRepository.getSize());
    }

    @Test
    public void testAddUserCollection() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES + 2;
        @NotNull List<User> users = new ArrayList<>();
        @NotNull final User user1 = new User();
        @NotNull final User user2 = new User();
        users.add(user1);
        users.add(user2);
        userRepository.add(users);
        Assert.assertEquals(expectedNumberOfEntries, userRepository.getSize());
    }

    @Test
    public void testClearUser() {
        final int expectedNumberOfEntries = 0;
        userRepository.clear();
        Assert.assertEquals(expectedNumberOfEntries, userRepository.getSize());
    }

    @Test
    public void testGetSizeUser() {
        Assert.assertEquals(userList.size(), userRepository.getSize());
    }

    @Test
    public void testSetUser() {
        @NotNull List<User> users = new ArrayList<>();
        @NotNull final User user1 = new User();
        @NotNull final User user2 = new User();
        users.add(user1);
        users.add(user2);
        final int expectedNumberOfEntries = users.size();
        Assert.assertEquals(expectedNumberOfEntries, userRepository.set(users).size());
    }

    @Test
    public void testFindAllUser() {
        Assert.assertEquals(userList.size(), userRepository.findAll().size());
    }

    @Test
    public void testFindUserById() {
        for (@NotNull final User user : userList) {
            Assert.assertNotNull(userRepository.findById(user.getId()));
            Assert.assertEquals(user, userRepository.findById(user.getId()));
        }
    }

    @Test
    public void testFindUserByIndex() {
        for (int i = 0; i < userRepository.getSize(); i++) {
            Assert.assertNotNull(userRepository.findByIndex(i));
            Assert.assertEquals(userList.get(i), userRepository.findByIndex(i));
        }
    }

    @Test
    public void testFindUserByLogin() {
        for (@NotNull final User user : userList) {
            Assert.assertNotNull(userRepository.findByLogin(user.getLogin()));
            Assert.assertEquals(user, userRepository.findByLogin(user.getLogin()));
        }
        Assert.assertNull(userRepository.findByLogin(null));
    }

    @Test
    public void testFindUserByEmail() {
        for (@NotNull final User user : userList) {
            Assert.assertNotNull(userRepository.findByEmail(user.getEmail()));
            Assert.assertEquals(user, userRepository.findByEmail(user.getEmail()));
            Assert.assertNull(userRepository.findByEmail(null));
        }
    }

    @Test
    public void testRemoveUser() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        @NotNull final User user = userList.get(0);
        userRepository.remove(user);
        Assert.assertEquals(expectedNumberOfEntries, userRepository.getSize());
        @Nullable final User user2 = null;
        Assert.assertNull(userRepository.remove(user2));
    }

    @Test
    public void testRemoveUserById() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        @NotNull final User user = userList.get(0);
        userRepository.removeById(user.getId());
        Assert.assertEquals(expectedNumberOfEntries, userRepository.getSize());
    }

    @Test
    public void testRemoveUserByIndex() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        final int index = 0;
        userRepository.removeByIndex(index);
        Assert.assertEquals(expectedNumberOfEntries, userRepository.getSize());
    }

    @Test
    public void testIsLoginExist() {
        @NotNull final String login = userList.get(0).getLogin();
        @NotNull final Boolean loginExist1 = userList.stream().anyMatch(u -> login.equals(u.getLogin()));
        @NotNull final Boolean loginExist2 = userRepository.isLoginExist(login);
        Assert.assertEquals(loginExist1, loginExist2);
    }

    @Test
    public void testIsEmailExist() {
        @NotNull final String email = userList.get(0).getEmail();
        @NotNull final Boolean emailExist1 = userList.stream().anyMatch(u -> email.equals(u.getEmail()));
        @NotNull final Boolean emailExist2 = userRepository.isEmailExist(email);
        Assert.assertEquals(emailExist1, emailExist2);
    }



}