package ru.t1.dkozoriz.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.dkozoriz.tm.api.repository.ISessionRepository;
import ru.t1.dkozoriz.tm.api.service.ISessionService;
import ru.t1.dkozoriz.tm.exception.entity.EntityException;
import ru.t1.dkozoriz.tm.exception.field.*;
import ru.t1.dkozoriz.tm.exception.user.UserIdEmptyException;
import ru.t1.dkozoriz.tm.model.Session;
import ru.t1.dkozoriz.tm.repository.SessionRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class SessionServiceTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private static final String USER_ID_1 = UUID.randomUUID().toString();

    @NotNull
    private static final String USER_ID_2 = UUID.randomUUID().toString();

    @NotNull
    private final List<Session> sessionList = new ArrayList<>();

    @NotNull
    private final ISessionRepository sessionRepository = new SessionRepository();

    @NotNull
    private final ISessionService sessionService = new SessionService(sessionRepository);

    @Before
    public void initRepository() {
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final Session session = new Session();
            if (i <= 5) session.setUserId(USER_ID_1);
            else session.setUserId(USER_ID_2);
            sessionRepository.add(session);
            sessionList.add(session);
        }
    }

    @Test
    public void testAddSession() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        @NotNull final Session session = new Session();
        sessionService.add(session);
        Assert.assertEquals(expectedNumberOfEntries, sessionService.getSize());
    }

    @Test
    public void testAddSessionNegative() {
        @Nullable final Session session = null;
        Assert.assertThrows(EntityException.class, () -> sessionService.add(session));
    }

    @Test
    public void testClearSession() {
        final int expectedNumberOfEntries = 0;
        sessionService.clear();
        Assert.assertEquals(expectedNumberOfEntries, sessionService.getSize());
    }

    @Test
    public void testClearUserOwnedSession() {
        @NotNull final List<Session> userSessionList = sessionList
                .stream()
                .filter(p -> USER_ID_1.equals(p.getUserId()))
                .collect(Collectors.toList());
        final int expectedNumberOfEntries = userSessionList.size();
        sessionService.clear(USER_ID_1);
        Assert.assertEquals(expectedNumberOfEntries, sessionService.getSize());
    }

    @Test
    public void testClearUserOwnedSessionNegative() {
        @Nullable final String userId1 = null;
        Assert.assertThrows(UserIdEmptyException.class, () -> sessionService.clear(userId1));
        @Nullable final String userId2 = "";
        Assert.assertThrows(UserIdEmptyException.class, () -> sessionService.clear(userId2));
    }

    @Test
    public void testFindAllSession() {
        Assert.assertEquals(sessionList.size(), sessionService.findAll().size());
    }

    @Test
    public void testFindAllUserOwnedSession() {
        @NotNull final List<Session> userSessionList = sessionList
                .stream()
                .filter(p -> USER_ID_1.equals(p.getUserId()))
                .collect(Collectors.toList());
        final int expectedNumberOfEntries = userSessionList.size();
        Assert.assertEquals(expectedNumberOfEntries, sessionService.findAll(USER_ID_1).size());
    }

    @Test
    public void testFindAllUserOwnedSessionNegative() {
        @Nullable final String userId = null;
        Assert.assertThrows(UserIdEmptyException.class, () -> sessionService.findAll(userId));
        @Nullable final String id2 = "";
        Assert.assertThrows(IdEmptyException.class, () -> sessionService.removeById(id2));
    }

    @Test
    public void testRemoveSession() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        @NotNull final Session session = sessionList.get(0);
        sessionService.remove(session);
        Assert.assertEquals(expectedNumberOfEntries, sessionService.getSize());
    }

    @Test
    public void testRemoveSessionNegative() {
        @Nullable final Session session = null;
        Assert.assertThrows(EntityException.class, () -> sessionService.remove(session));
    }

    @Test
    public void testRemoveUserOwnedSession() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        @NotNull final Session session = sessionList.get(0);
        sessionRepository.remove(USER_ID_1, session);
        Assert.assertEquals(expectedNumberOfEntries, sessionService.getSize());
    }

    @Test
    public void testRemoveUserOwnedSessionNegative() {
        @Nullable final String userId = null;
        Assert.assertThrows(UserIdEmptyException.class, () -> sessionService.remove(userId, sessionList.get(0)));
        @Nullable final Session session = null;
        Assert.assertThrows(EntityException.class, () -> sessionService.remove(USER_ID_1, session));
    }

    @Test
    public void testRemoveSessionById() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        @NotNull final Session session = sessionList.get(0);
        sessionService.removeById(session.getId());
        Assert.assertEquals(expectedNumberOfEntries, sessionService.getSize());
    }

    @Test
    public void testRemoveSessionByIdNegative() {
        @Nullable final String id = null;
        Assert.assertThrows(IdEmptyException.class, () -> sessionService.removeById(id));
    }

    @Test
    public void testRemoveUserOwnedSessionById() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        @NotNull final Session session = sessionList.get(0);
        sessionService.removeById(USER_ID_1, session.getId());
        Assert.assertEquals(expectedNumberOfEntries, sessionService.getSize());
    }

    @Test
    public void testRemoveUserOwnedSessionByIdNegative() {
        @Nullable final String userId = null;
        @Nullable final String sessionId = sessionList.get(0).getId();
        Assert.assertThrows(UserIdEmptyException.class, () -> sessionService.removeById(userId, sessionId));
        @Nullable final String id = null;
        Assert.assertThrows(IdEmptyException.class, () -> sessionService.existsById(USER_ID_1, id));
    }

    @Test
    public void testRemoveUserOwnedSessionByIndex() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        final int index = 0;
        sessionService.removeByIndex(USER_ID_1, index);
        Assert.assertEquals(expectedNumberOfEntries, sessionService.getSize());
    }

    @Test
    public void testRemoveUserOwnedSessionByIndexNegative() {
        @Nullable final String userId = null;
        Assert.assertThrows(UserIdEmptyException.class, () -> sessionService.removeByIndex(userId, 0));
        @Nullable final Integer index = -1;
        Assert.assertThrows(IndexIncorrectException.class, () -> sessionService.removeByIndex(USER_ID_1, index));
    }


    @Test
    public void testFindSessionById() {
        for (@NotNull final Session session : sessionList) {
            Assert.assertNotNull(sessionService.findById(session.getId()));
            Assert.assertEquals(session, sessionService.findById(session.getId()));
        }
    }

    @Test
    public void testFindUserOwnedSessionById() {
        @NotNull final List<Session> userSessionList = sessionList
                .stream()
                .filter(p -> USER_ID_1.equals(p.getUserId()))
                .collect(Collectors.toList());
        for (@NotNull final Session session : userSessionList) {
            Assert.assertNotNull(sessionService.findById(USER_ID_1, session.getId()));
            Assert.assertEquals(session, sessionService.findById(USER_ID_1, session.getId()));
        }
    }

    @Test
    public void testFindUserOwnedSessionByIdNegative() {
        @Nullable final String userId = null;
        @Nullable final String sessionId = sessionList.get(0).getId();
        Assert.assertThrows(UserIdEmptyException.class, () -> sessionService.findById(userId, sessionId));
        @Nullable final String id = null;
        Assert.assertThrows(IdEmptyException.class, () -> sessionService.findById(USER_ID_1, id));
    }

    @Test
    public void testFindSessionByIdNegative() {
        @Nullable final String id = null;
        Assert.assertThrows(IdEmptyException.class, () -> sessionService.findById(id));
    }

    @Test
    public void testFindUserOwnedSessionByIndex() {
        @NotNull final List<Session> userSessionList = sessionList
                .stream()
                .filter(p -> USER_ID_1.equals(p.getUserId()))
                .collect(Collectors.toList());

        for (int i = 0; i < userSessionList.size(); i++) {
            Assert.assertNotNull(sessionService.findByIndex(USER_ID_1, i));
            Assert.assertEquals(sessionList.get(i), sessionService.findByIndex(USER_ID_1, i));
        }
    }

    @Test
    public void testFindUserOwnedSessionByIndexNegative() {
        @Nullable final String userId = null;
        Assert.assertThrows(UserIdEmptyException.class, () -> sessionService.findByIndex(userId, 0));
        @Nullable final Integer index = null;
        Assert.assertThrows(IndexIncorrectException.class, () -> sessionService.findByIndex(USER_ID_1, index));
    }

    @Test
    public void testGetSizeSession() {
        Assert.assertEquals(sessionList.size(), sessionService.getSize());
    }

    @Test
    public void testGetSizeUserOwnedSession() {
        @NotNull final List<Session> userSessionList = sessionList
                .stream()
                .filter(p -> USER_ID_1.equals(p.getUserId()))
                .collect(Collectors.toList());
        Assert.assertEquals(userSessionList.size(), sessionService.getSize(USER_ID_1));
    }

    @Test
    public void testSetSession() {
        @NotNull List<Session> sessions = new ArrayList<>();
        @NotNull final Session session1 = new Session();
        @NotNull final Session session2 = new Session();
        sessions.add(session1);
        sessions.add(session2);
        final int expectedNumberOfEntries = sessions.size();
        Assert.assertEquals(expectedNumberOfEntries, sessionService.set(sessions).size());
    }

    @Test
    public void testAddSessionCollection() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES + 2;
        @NotNull List<Session> sessions = new ArrayList<>();
        @NotNull final Session session1 = new Session();
        @NotNull final Session session2 = new Session();
        sessions.add(session1);
        sessions.add(session2);
        sessionService.add(sessions);
        Assert.assertEquals(expectedNumberOfEntries, sessionService.getSize());
    }

    @Test
    public void testExistSessionById() {
        @NotNull final List<Session> userSessionList = sessionList
                .stream()
                .filter(p -> USER_ID_1.equals(p.getUserId()))
                .collect(Collectors.toList());
        for (@NotNull final Session session : userSessionList) {
            Assert.assertTrue(sessionService.existsById(USER_ID_1, session.getId()));
        }
    }

    @Test
    public void testExistSessionByIdNegative() {
        @Nullable final String userId = null;
        @Nullable final String sessionId = sessionList.get(0).getId();
        Assert.assertThrows(UserIdEmptyException.class, () -> sessionService.existsById(userId, sessionId));
        @Nullable final String id = null;
        Assert.assertThrows(IdEmptyException.class, () -> sessionService.existsById(USER_ID_1, id));
    }

}