package ru.t1.dkozoriz.tm.command.data.load;

import lombok.SneakyThrows;
import ru.t1.dkozoriz.tm.command.data.AbstractDataCommand;
import ru.t1.dkozoriz.tm.dto.request.data.load.DataJsonLoadJaxBRequest;
import ru.t1.dkozoriz.tm.dto.request.data.load.DataXmlLoadJaxBRequest;

public final class DataXmlLoadJaxBCommand extends AbstractDataCommand {

    public DataXmlLoadJaxBCommand() {
        super("data-load-xml-jaxb", "load data from xml file.");
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA LOAD XML]");
        getEndpointLocator().getDomainEndpoint().loadXmlJaxB(new DataXmlLoadJaxBRequest(getToken()));
    }

}