package ru.t1.dkozoriz.tm.command.data.save;

import lombok.SneakyThrows;
import ru.t1.dkozoriz.tm.command.data.AbstractDataCommand;
import ru.t1.dkozoriz.tm.dto.request.data.save.DataBase64SaveRequest;
import ru.t1.dkozoriz.tm.dto.request.data.save.DataJsonSaveJaxBRequest;

public final class DataJsonSaveJaxBCommand extends AbstractDataCommand {

    public DataJsonSaveJaxBCommand() {
        super("data-save-json-jaxb", "save data in json file.");
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA SAVE JSON]");
        getEndpointLocator().getDomainEndpoint().saveJsonJaxB(new DataJsonSaveJaxBRequest(getToken()));
    }

}