package ru.t1.dkozoriz.tm.command.data.load;

import lombok.SneakyThrows;
import ru.t1.dkozoriz.tm.command.data.AbstractDataCommand;
import ru.t1.dkozoriz.tm.dto.request.data.load.DataBase64LoadRequest;
import ru.t1.dkozoriz.tm.dto.request.data.load.DataJsonLoadFasterRequest;

public final class DataJsonLoadFasterCommand extends AbstractDataCommand {

    public DataJsonLoadFasterCommand() {
        super("data-load-json", "load data from json file.");
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA LOAD JSON]");
        getEndpointLocator().getDomainEndpoint().loadJsonFaster(new DataJsonLoadFasterRequest(getToken()));
    }

}