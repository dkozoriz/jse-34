package ru.t1.dkozoriz.tm.dto.request.data.load;

import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.dto.request.AbstractUserRequest;

public class DataBackupLoadRequest extends AbstractUserRequest {

    public DataBackupLoadRequest(@Nullable final String token) {
        super(token);
    }

}