package ru.t1.dkozoriz.tm.dto.request.data.load;

import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.dto.request.AbstractUserRequest;

public class DataXmlLoadJaxBRequest extends AbstractUserRequest {

    public DataXmlLoadJaxBRequest(@Nullable final String token) {
        super(token);
    }

}