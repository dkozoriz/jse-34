package ru.t1.dkozoriz.tm.dto.response.project;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.dto.response.AbstractResponse;
import ru.t1.dkozoriz.tm.model.business.Project;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UpdateProjectByIdResponse extends AbstractResponse {

    @Nullable
    private Project project;

}