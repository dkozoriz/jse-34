package ru.t1.dkozoriz.tm.dto.request.data.save;

import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.dto.request.AbstractUserRequest;

public class DataJsonSaveFasterRequest extends AbstractUserRequest {

    public DataJsonSaveFasterRequest(@Nullable final String token) {
        super(token);
    }

}