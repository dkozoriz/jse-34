package ru.t1.dkozoriz.tm.dto.request.data.load;

import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.dto.request.AbstractUserRequest;

public class DataBase64LoadRequest extends AbstractUserRequest {

    public DataBase64LoadRequest(@Nullable final String token) {
        super(token);
    }

}